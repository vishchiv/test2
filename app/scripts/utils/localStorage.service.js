(function() {
    'use strict';

    angular
        .module('test2')
        .factory('angularLocalStorage', angularLocalStorage);

    angularLocalStorage.$inject = ['$window'];

    function angularLocalStorage($window) {

        var _prefix = 'ng';
        var _localStorage = $window.localStorage;

        var service = {
            setPrefix: setPrefix,
            setItem: setItem,
            getItem: getItem,
            getAllItems: getAllItems,
            removeItem: removeItem,
            clear: clear
        };

        return service;

        function addPrefixToKey(key) {
            return _prefix + '-' + key;
        }

        function removePrefixFromKey(keyWithPrefix) {
            return keyWithPrefix.replace(_prefix + '-', '');
        }

        function setPrefix(prefixToSet) {
            _prefix = prefixToSet;
        }

        function setItem(key, value) {
            try {
                value = JSON.stringify(value)
            } finally {
                _localStorage.setItem(addPrefixToKey(key), value);
            }


        }

        function getItem(key) {
            var item = _localStorage.getItem(addPrefixToKey(key));

            try {
                return JSON.parse(item);
            } catch (e) {
                return item;
            }
        }

        function removeItem(key) {
            _localStorage.removeItem(addPrefixToKey(key));
        }

        /**
         * Returns all items with current prefix
         *
         * @returns {Array}
         */
        function getAllItems() {
            var allItems = [];

            for (var i = 0; i < _localStorage.length; i++) {
                var key = _localStorage.key(i);

                if (key.startsWith(_prefix)) {
                    allItems.push(getItem(removePrefixFromKey(key)));
                }
            }

            return allItems;
        }


        /**
         * Removes all items with current prefix
         *
         */
        function clear() {
            // remove items that are relative to current app
            for (var i = 0; i < _localStorage.length; i++) {
                var key = _localStorage.key(i);

                if (key.startsWith(_prefix)) {
                    allItems.push(getItem(removePrefixFromKey(key)));
                }
            }
        }

    }

})();
