(function() {
    'use strict';

    angular
        .module('test2')
        .factory('UUID', UUID);

    UUID.$inject = [];

    function UUID() {

        var prefix = 'ng'

        var service = {
            uuid: uuid
        };

        return service;

        //simulates uuid
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        function uuid() {
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

    }

})();
