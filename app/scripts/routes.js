(function() {
    'use strict';

    angular
        .module('test2')
        .config(routeConfig);

    /** @ngInject */
    routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routeConfig($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/list');

        $stateProvider
            .state('list', {
                url: '/list',
                templateUrl: 'views/list.view.html',
                controller: 'ListController',
                controllerAs: 'vm'
            })
            .state('add-item', {
                url: '/list/add',
                templateUrl: 'views/add-item.view.html',
                controller: 'AddItemController',
                controllerAs: 'vm'
            })
            .state('edit-item', {
                url: '/list/edit/:id',
                templateUrl: 'views/edit-item.view.html',
                controller: 'EditItemController',
                controllerAs: 'vm'
            })
    }

})();
