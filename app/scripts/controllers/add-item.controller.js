(function() {
    'use strict';

    angular
        .module('test2')
        .controller('AddItemController', AddItemController);

    /** @ngInject */
    AddItemController.$inject = ['angularLocalStorage', 'UUID', '$state'];

    function AddItemController(angularLocalStorage, UUID, $state) {
        var vm = this;

        vm.item = {
            id: UUID.uuid(),
            name: ''
        }

        vm.saveItem = saveItem;

        function saveItem() {
            angularLocalStorage.setItem(vm.item.id, vm.item);
            $state.go('list');
        }
    }

})();
