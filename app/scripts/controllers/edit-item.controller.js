(function() {
    'use strict';

    angular
        .module('test2')
        .controller('EditItemController', EditItemController);

    /** @ngInject */
    EditItemController.$inject = ['$state', '$stateParams', 'angularLocalStorage'];

    function EditItemController($state, $stateParams, angularLocalStorage) {
        var vm = this;

        var itemId = $stateParams.id;

        vm.item = angularLocalStorage.getItem(itemId);

        vm.saveItem = saveItem;

        function saveItem() {
            angularLocalStorage.setItem(vm.item.id, vm.item);
            $state.go('list');
        }
    }

})();
