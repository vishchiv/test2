(function() {
    'use strict';

    angular
        .module('test2')
        .controller('ListController', ListController);

    /** @ngInject */
    ListController.$inject = ['angularLocalStorage'];

    function ListController(angularLocalStorage) {
        var vm = this;

        vm.items = angularLocalStorage.getAllItems();
        vm.removeItem = removeItem;

        function removeItem(id) {
            angularLocalStorage.removeItem(id);
            vm.items = angularLocalStorage.getAllItems()
        }

    }

})();
